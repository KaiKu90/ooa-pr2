//
// Created by Kai Kuhlmann on 19.04.16.
//

#include <iostream>
#include "pqueue.h"

#define MAX 100000

using namespace std;

int main() {
    Queue<int> *queue;

    int valInt1[] = {1, 2, 3, 4, 5, 6};
    int valInt2[] = {7, 8, 9, 10};

    float prio1[] = {15, 30, 10, 45, 5, 40};
    float prio2[] = {25, 20, 35, 50};

    clock_t tic, toc;



    /**
     * QUEUE TEST
     */

    queue = new Queue<int>();

    cout << "\nInsert some entries to queue" << endl;
    for (int i = 0; i < 6; i++) {
        queue->insert(valInt1[i], prio1[i]);
    }
    queue->printQueue();

    cout << "\nInsert some more entries to queue" << endl;
    for (int i = 0; i < 4; i++) {
        queue->insert(valInt2[i], prio2[i]);
    }
    queue->printQueue();

    try {
        cout << "\nExtract value with highest priority in queue" << endl;
        cout << "Value = " << queue->extractMin() << endl;
        queue->printQueue();
    } catch (QueueException e) {
        cout << e.getError() << endl;
    }

    try {
        cout << "\nDecrease priority-value of value \"10\"" << endl;
        queue->decreaseKey(valInt2[3], 1);
        queue->printQueue();
    } catch (QueueException e) {
        cout << e.getError() << endl;
    }

    cout << "\nRemove entry with value \"4\"" << endl;
    queue->remove(valInt1[3]);
    queue->printQueue();

    delete(queue);
    cout << "\nQueue successfully destroyed" << endl;



    /**
     * EXCEPTIONS TEST
     */

    queue = new Queue<int>();

    cout << "\nQueueExceptions test for \"extractMin\"" << endl;
    try {
        cout << "Extract value with highest priority in queue" << endl;
        cout << "Value = " << queue->extractMin() << endl;
        queue->printQueue();
    } catch (QueueException e) {
        cout << e.getError() << endl;
    }

    cout << "\nQueueExceptions test for \"decreaseKey\"" << endl;
    try {
        cout << "Decrease priority-value of value \"20\"" << endl;
        queue->decreaseKey(20, 1);
        queue->printQueue();
    } catch (QueueException e) {
        cout << e.getError() << endl;
    }

    delete(queue);
    cout << "\nQueue successfully destroyed" << endl;



    /**
     * TIME MEASURING
     */

    queue = new Queue<int>();

    cout << "\nMeasuring time for function pqueue_insert() ..." << endl;
    tic = clock();
    for (int i = 0; i < MAX; ++i) {
        queue->insert(rand() % 1000, rand() % 100);
    }
    toc = clock();
    cout << "Insertion time: " << ((float) (toc-tic) / CLOCKS_PER_SEC) << endl;

    cout << "\nMeasuring time for function pqueue_extractMin() ..." << endl;
    tic = clock();
    for (int i = 0 ; i < MAX ; ++i) {
        queue->extractMin();
    }
    toc = clock();
    cout << "Extract time: " << ((float) (toc-tic) / CLOCKS_PER_SEC) << endl;

    delete(queue);
    cout << "\nQueue successfully destroyed\n" << endl;

    return EXIT_SUCCESS;
}