//
// Created by Kai Kuhlmann on 19.04.16.
//

#ifndef OOA_PR2_QUEUE_H
#define OOA_PR2_QUEUE_H

#include <iostream>
#include <string>
#include "queue_exceptions.h"

using namespace std;

template <typename T>
class Queue {

private:
    struct pqentry {
        T value;
        float priority;
        pqentry *next;
    };

    pqentry *entry;

    bool isEmpty();

public:
    Queue();
    ~Queue();

    void insert(T value, float priority);
    void decreaseKey(T value, float priority);
    void remove(T value);
    T extractMin(void);
    void printQueue();
};

template <typename T>
Queue<T>::Queue() {
    entry = nullptr;
}

template <typename T>
Queue<T>::~Queue() {
    delete[] entry;
}

template <typename T>
void Queue<T>::insert(T value, float priority) {
    pqentry *newEntry = new pqentry;
    pqentry *current = entry;

    newEntry->value = value;
    newEntry->priority = priority;
    newEntry->next = nullptr;

    if (isEmpty()) {
        entry = newEntry;
    } else {
        if (entry->priority > priority) {
            newEntry->next = entry;
            entry = newEntry;
        } else {
            while (current->next != nullptr && current->next->priority < priority) {
                current = current->next;
            }

            newEntry->next = current->next;
            current->next = newEntry;
        }
    }
}

template <typename T>
void Queue<T>::decreaseKey(T value, float priority) {
    pqentry *current = entry;

    if (current == nullptr) throw QueueException("ENTRY NOT FOUND");

    while (current->next != nullptr && current->next->value != value) {
        current = current->next;
    }

    if (current->next != nullptr) {
        remove(value);
        insert(value, priority);
    } else throw QueueException("ENTRY NOT FOUND");
}

template <typename T>
void Queue<T>::remove(T value) {
    pqentry *current = entry;

    if (current->value == value) {
        entry = current->next;
        delete(current);
        return;
    }

    while (current->next != nullptr) {
        if (current->next->value == value) {
            current->next = current->next->next;
            delete(current->next);
            break;
        }

        current = current->next;
    }
}

template <typename T>
T Queue<T>::extractMin(void) {
    if (isEmpty()) throw QueueException("QUEUE IS EMPTY");

    pqentry *current = entry;
    T tmpVal;

    tmpVal = current->value;
    remove(current->value);
    return tmpVal;
}

template <typename T>
void Queue<T>::printQueue() {
    pqentry *current = entry;

    for (int i = 0; current != nullptr; i++, current = current->next) {
        cout <<
        "Queue[" << i << "]:" <<
        " Value = " << current->value <<
        " Priority = " << current->priority << endl;
    }
}

template <typename T>
bool Queue<T>::isEmpty() {
    return entry == nullptr;
}


#endif //OOA_PR2_QUEUE_H
