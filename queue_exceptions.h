//
// Created by Kai Kuhlmann on 19.04.16.
//

#ifndef OOA_PR2_QUEUE_EXCEPTIONS_H
#define OOA_PR2_QUEUE_EXCEPTIONS_H

#include <string>
using namespace std;

class QueueException {

private:
    string error;

public:
    QueueException(string error);
    string getError();
};

QueueException::QueueException(string error) {
    this->error = error;
}

string QueueException::getError() {
    return "ERROR: " + this->error;
}

#endif //OOA_PR2_QUEUE_EXCEPTIONS_H
